let canvas;
let ctx;
let p;
let ball;
let grid;

let UNIT_W;
let UNIT_H;

let oldWidth;
let oldHeight;

/////////////////////////////////////////
// Define Objects
/////////////////////////////////////////
class Paddle {
  constructor(){
    console.log("Constructor");
    this.momentum = 0;
    this.w = UNIT_W * 4;
    this.h = UNIT_H;
    
    this.x = (canvas.width / 2) - (this.w / 2);
    this.y = canvas.height - (2 * this.h);
  }

  resize(){
    this.w = UNIT_W * 4;
    this.h = UNIT_H;
    this.y = canvas.height - (2 * this.h);
    this.x = (this.x/oldWidth) * canvas.width;
  }

  move(dx){
    console.log("Moving");
    this.x += dx;
  }

  draw(){
    console.log("Drawing");
    ctx.fillStyle = "#ccffcc";
    ctx.fillRect(
      this.x,
      this.y,
      this.w,
      this.h
    );
  }

};

class Block {
  constructor(x, y){
    this.w = 2 * UNIT_W;
    this.h = UNIT_H;
    this.x = x;
    this.y = y;
    this.hit = false;
  }

  resize(){
    this.w = 2 * UNIT_W;
    this.h = UNIT_H;

    this.x = (this.x/oldWidth) * canvas.width;
    this.y = (this.y/oldHeight) * canvas.height;
  }

  update(){
    if(!this.hit){
      if(ball.x > this.x 
      && ball.x < this.x + this.w 
      && ball.y > this.y
      && ball.y < this.y+this.h){
        this.hit = true;
        ball.dx *= 1.02;
        ball.dy *= -1.02;
        ctx.fillStyle = "orange";
        ctx.fillRect(
          this.x,
          this.y,
          this.w,
          this.h
        );
      }
    }
  }

  draw(){
    if(!this.hit){
      ctx.fillStyle = "#fff";
      ctx.fillRect(
        this.x,
        this.y,
        this.w,
        this.h
      );
    }
  }
};

class Grid {
  constructor(){
    this.row = 9;
    this.col = 5;
    this.w = this.row * 3 - 1;
    this.x = (canvas.width / 2) - ((this.w / 2) * UNIT_W);
    this.y = UNIT_H;
    this.blocks = [];
    for(var i = 0; i < this.col; i++){
      var tmp = [];
      for(var j = 0; j < this.row; j++){
        tmp.push(new Block(j * UNIT_W * 3 + this.x, i * UNIT_H * 2 + this.y));
      }
      this.blocks.push(tmp);
    }
  }

  resize(){
    for(var i = 0; i < this.col; i++){
      for(var j = 0; j < this.row; j++){
        this.blocks[i][j].resize();
      }
    }
  }

  update(){
    for(var i = 0; i < this.col; i++){
      for(var j = 0; j < this.row; j++){
        this.blocks[i][j].update();
      }
    }
  }

  draw(){
    for(var i = 0; i < this.col; i++){
      for(var j = 0; j < this.row; j++){
        this.blocks[i][j].draw();
      }
    }
  }
};

class Ball {
  constructor(){
    this.x = 0;
    this.y = 0;
    this.dx =  2;
    this.dy = -2;
    this.radius = UNIT_W / 4;
    this.reset = true;
  }

  resize(){
    this.radius = UNIT_W / 4;
    this.x = (this.x/oldWidth) * canvas.width;
    this.y = (this.y/oldHeight) * canvas.height;
  }

  update(){
    if(this.reset){
      this.x = p.x + p.w / 2;
      this.y = p.y - 2*UNIT_H;
      this.dx =  2;
      this.dy = -2;
    } else {
      if(this.x > canvas.width || this.x < 0){
        this.dx *= -1;
      }

      // Ball collides with roof
      if(this.y < 0){
        this.dy *= -1;
      } // Ball collides with paddle 
      else if(this.y > p.y && this.x > p.x && this.x < p.x+p.w){
        this.dy = -Math.abs(this.dy);
        this.dx += p.momentum;
      } // Ball collides with floor
      else if(this.y > canvas.height){
        this.reset = true;
        return;
      }

      this.x += this.dx;
      this.y += this.dy;
    }
  }

  draw(){
    ctx.fillStyle = "white";
    ctx.beginPath();
    ctx.ellipse(this.x, this.y, this.radius, this.radius, 0, 0, 2*Math.PI);
    ctx.fill();
  }
};

/////////////////////////////////////////
// Initialize Game
/////////////////////////////////////////
window.onload = () => {
  canvas = document.querySelector("canvas");
  ctx = canvas.getContext("2d");
  
  window.addEventListener("keydown", callback, true);
  
  canvas.style.backgroundColor = "#bbbbff";

  canvas.width = window.innerWidth * 0.8;
  canvas.height = canvas.width * 9/16;
  
  UNIT_H = canvas.height / 18;
  UNIT_W = canvas.width  / 32;

  // Create and Initialize objects
  p = new Paddle();
  grid = new Grid();
  ball = new Ball();
  
  // Set update loop
  setInterval(update, 1000/60);
}

/////////////////////////////////////////
// Resize Event
/////////////////////////////////////////
window.onresize = () => {
  // Keep a 16:9 aspect ratio
  oldWidth = canvas.width;
  oldHeight = canvas.height;

  canvas.width = window.innerWidth * 0.8;
  canvas.height = canvas.width * 9/16;

  UNIT_H = canvas.height / 18;
  UNIT_W = canvas.width  / 32;

  p.resize();
  ball.resize();
  grid.resize();
}

/////////////////////////////////////////
// Process Input
/////////////////////////////////////////
const callback = (e) => {
  switch(e.keyCode){
    case 32: // SPACE
      ball.reset = false;
      break;
    case 37: // LEFT
      p.move(-20);
      p.momentum = -0.2;
      break;
    case 39: // RIGHT
      p.move(20);
      p.momentum = 0.2;
      break;
    default:

      break;
  }
}

/////////////////////////////////////////
// Update Game Loop
/////////////////////////////////////////
const update = () => {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  

  grid.update();
  ball.update();

  p.draw();
  grid.draw();
  ball.draw();
}